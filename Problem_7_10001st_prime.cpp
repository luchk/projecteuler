#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
using namespace std;

bool isPrime(int n) 
{ 
    if (n <= 1) 
        return false; 

    for (int i = 2; i < n; i++) 
        if (n % i == 0) 
            return false; 

    return true; 
}

int main(void)
{
	int counter = 0;
	long long int checkPrime = 0;
	while(counter != 10001)
	{
		checkPrime++;
		if(isPrime(checkPrime))
			counter++;
	}
	cout << checkPrime;
	return 0;
}

// answer 104743
