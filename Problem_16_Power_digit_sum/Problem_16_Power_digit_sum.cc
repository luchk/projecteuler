// Sample program demonstrating the use of the Big Integer Library.

// Standard libraries
#include <string>
#include <iostream>

#include "BigIntegerLibrary.hh"


using namespace std;

BigInteger findSum(BigInteger a)
{
	// int b;
	BigInteger sum = 0;
	BigInteger last = a;
	while(last != 0)
	{
		sum += last % 10;
		last /= 10;
	}
	return sum;
}



int main() {
	try {
		BigInteger number = 2;
	for (int i = 0; i < 999; ++i)
	{
		number *= 2;
		// cout << number << endl;
	}

	cout << findSum(number);
	} catch(char const* err) {
		std::cout << "The library threw an exception:\n"
			<< err << std::endl;
	}


	return 0;
}



// answer 1366