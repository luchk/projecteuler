#include "stdc++.h"
#define __int64 __int64_t
// #define __int64 long long int 

using namespace std;

__int64 findSum(int a)
{
	__int64 sum = 0;
	if(a <= 0)
		return 0;
	for (int i=1; i<=sqrt(a); i++)
	{
		if (a%i == 0)
		{
			if (a/i == i)
				sum += i;
			else
				sum += i + a/i;
		}
	}
	return sum-a;
}


 
__int64 sumAmicablePairs(int low, int high)
{
	int b;
	int sum = 0;
	for (int a = low; a <= high; ++a)
	{
		b = findSum(a);
		if(b > a && findSum(b) == a){
			sum += a + b;
		}
	}
	return sum;
}

int main(int argc, char const *argv[])
{
	__int64 sumOfAll = 0;
	sumOfAll = sumAmicablePairs(1, 10000);
	cout <<sumOfAll ;
	return 0;
}

// ans 31626