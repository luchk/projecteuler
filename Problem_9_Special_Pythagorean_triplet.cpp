#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
#include <cmath>
using namespace std;


bool haveRoot(int a)
{
	return ((int)sqrt(a) == sqrt(a)) ? 1 : 0;
}

bool small(int a, int b, int c)
{
	return (a < b) && (b < c) ? 1 : 0;
}

bool check(int a, int b, int c)
{
	return (a + b + c == 1000) ? 1 : 0;
}

bool checkPower(int a, int b, int c)
{
	return (a*a + b*b == c*c) ? 1 : 0;
}

int main(int argc, char const *argv[])
{
	for (int i = 0; i < 1000; ++i)
	{
		for (int j = 0; j < 1000; ++j)
		{
			for (int k = 0; k < 1000; ++k)
			{
				if(check(i, j, k)){
					if(small(i, j, k)){
						if(checkPower(i, j, k))
							cout<< i << "  " << j << "  " << k << "  "<< i*j*k << endl;
					}
				}
			}
		}
	}

	return 0;
}


// answer  31875000