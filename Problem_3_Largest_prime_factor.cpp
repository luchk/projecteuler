#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
using namespace std;

bool isPrime(int n) 
{ 
    if (n <= 1) 
        return false; 

    for (int i = 2; i < n; i++) 
        if (n % i == 0) 
            return false; 

    return true; 
}


int main(void)
{
	long long int testNum = 600851475143;
	for (long long int i = 0; i < testNum; ++i)
	{
		if(isPrime(i) && testNum % i == 0)
			cout << i << endl;
	}

}

// answer 6857