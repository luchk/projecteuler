#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
using namespace std;

int main(void)
{
	unsigned long long int sumOfTheSquares = 0;
	unsigned long long int squareOfTheSum  = 0;
	for (int i = 0; i <= 100; ++i)
	{
		sumOfTheSquares += i*i;
		squareOfTheSum += i;
	}
	cout << squareOfTheSum*squareOfTheSum - sumOfTheSquares;
	return 0;
}

// answer 25164150