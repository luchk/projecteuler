#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
using namespace std;


bool isPalindrome(int pal)
{
	int temp = pal;
	int reverse = 0, remainder;
   while( temp != 0)
   {
      remainder = temp % 10;
      reverse = reverse * 10 + remainder;
      temp = temp / 10;
   }
   if (pal == reverse){
      // cout << "\n" << pal << " is a palindrome number." << endl;
   	return 1;
   }
   else{
      // cout << "\n" << pal << " is not a palindrome number." << endl;
   return 0;
   }
}

int main(void)
{
	int largestPalindrome = 0;
	for (int i = 100; i < 999; ++i)
	{
		for (int k = 100; k < 999; ++k)
		{
			if(isPalindrome(i*k)){
				if(i*k > largestPalindrome)
					largestPalindrome = i*k;
			}
		}
	}
	cout << largestPalindrome;
   return 0;
}


// answer 906609