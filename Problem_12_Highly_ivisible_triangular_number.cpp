#include "stdc++.h"
using namespace std;

unsigned long long int generateTriangularNumber(int a)
{
	unsigned long long int number = 0;
	for (int i = 0; i <= a; ++i)
	{
		number += i;
	}
	return number;
}

int findNumberOfDivisors(long long int n)
{
	int number = 0;
	for (long long int i = 1; i <= sqrt(n); i++)
	{
		if (n % i == 0) {
			if (n / i == i){
				number++;
			}

			else
				number += 2;
		}
	}
	return number;
}


int main(int argc, char const *argv[])
{
	int numberOfDivisors = 1;
	int counter = 1;
	while(numberOfDivisors < 500)
	{
		counter++;
		numberOfDivisors = findNumberOfDivisors(generateTriangularNumber(counter));
		if(counter % 1000 == 0)
			cout << counter << endl;
	}
	cout << generateTriangularNumber(counter) << "  " << counter;
	return 0;
}

// answer 76576500