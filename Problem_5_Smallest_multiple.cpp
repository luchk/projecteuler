#include<iostream>
#include<stdio.h>
#include<iomanip>
#include <climits>
using namespace std;

bool smallestMultiple(long long int k)
{
	for (int i = 1; i <= 20; ++i)
	{
		if (k % i != 0){
			return false;
		}
	}
	return true;
}

int main(void)
{
	long long int a = 2520;
	while(!smallestMultiple(a))
		a++;
	cout << a;
	return 0;
}


// answer 232792560