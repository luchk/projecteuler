#include "stdc++.h"
#include <gmpxx.h>
#define __int64 __int64_t
// #define __int64 long long int 

using namespace std;

int main(int argc, char const *argv[])
{
	int length = 0;
	__int64 index = 4;
	mpz_t a;
	mpz_t b;
	mpz_t c;
	mpz_init(a);
	mpz_init(b);
	mpz_init(c);
	mpz_set_ui(a, 1);
	mpz_set_ui(b, 1);
	mpz_add(c, a, b);

	while(length <1000)
	{
		mpz_set(a, b);
		mpz_set(b, c);
		mpz_set(b, c);
		mpz_add(c, a, b);
		length = mpz_sizeinbase(c, 10);
		index++;
	}
	mpz_out_str(stdout,10 ,c);
	cout << "\n";
	mpz_clear(c);
	cout << index;

	return 0;
}

// answer 4782